#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /Sanity/dir-install
#   Description: Install using OAA into a directory, check that the results file from the final scan contains FIXED results, which proves that remediations were executed.
#   Author: Matej Tyc <matyc@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="oscap-anaconda-addon"
KS=`pwd`/kickstart.cfg
INSTALLDIR=`pwd`/install


function get_fedora_repo_url {
    local rawhide_repo="/etc/yum.repos.d/fedora-rawhide.repo"

    if [ -f "$rawhide_repo" ]; then
        dnf repoinfo rawhide 2>/dev/null | grep -i "baseurl" | awk '{print $3}'
    else
        dnf repoinfo fedora 2>/dev/null | grep -i "baseurl" | awk '{print $3}'
    fi
}


# $1: Channel (baseos|appstream)
function get_rhel_repo_url {
	grep -Ri "baseurl=.*$1.*/os" /etc/yum.repos.d/ | sed -e 's/.*baseurl=//' | head -n 1
}


rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm "$PACKAGE"
        if rlIsRHEL; then
            baseos_url=$(get_rhel_repo_url baseos)
            appstream_url=$(get_rhel_repo_url appstream)
            rlRun "sed -i 's|@BASEOS_HTTP@|$baseos_url|' $KS"
            rlRun "sed -i 's|@APPSTREAM_HTTP@|$appstream_url|' $KS"
        else
            baseos_url=$(get_fedora_repo_url)
            rlRun "sed -i 's|@BASEOS_HTTP@|$baseos_url|' $KS"
            rlRun "sed -i '/^.*@APPSTREAM_HTTP@.*$/d' $KS"
        fi
        python3 -m http.server &
        server_pid=$!
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "mkdir install" 0 "Making install directory"
        rlRun "anaconda --dirinstall $INSTALLDIR --kickstart $KS" 0 "Installing into a directory"
        rlRun "test -f $INSTALLDIR/rh_baseos_test" 0 "Make sure that rh_baseos_test exists in the root of the installed system"
    rlPhaseEnd

    rlPhaseStartCleanup
       rlRun "kill $server_pid" 0 "Terminating the Python server that serves the datastream"
       rlRun "rm -rf $INSTALLDIR" 0 "Remove the directory with the system installation"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
